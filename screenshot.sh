#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

## Modification : Dam0k1es
## Added Multiscreen Support
## Added Video Support
## Shellchecked

## Note: Selecting a Display is not usefull on i3
## since everything is drawn on :0

dir="$HOME/.config/rofi/"
rofi_command="rofi -theme $dir/five.rasi"

select_display() {
    rofi -dmenu\
        -i\
        -no-fixed-num-lines\
        -p "Enter Display : "\
        -theme "$dir"/confirm.rasi
    }

# Error msg
msg() {
    rofi -theme "$dir/message.rasi" -e "Please install 'flameshot (Xorg)' or 'grim (Wayland)' first."
}

# Options
dualscreen=""
screen="󰍹"
area="󱊅"
window=""
video="󰯜"

# Variable passed to rofi
options="$dualscreen\n$screen\n$area\n$window\n$video"

chosen="$(echo -e "$options" | $rofi_command -p '' -dmenu -selected-row 1)"
case "$chosen" in
    "$dualscreen")
        if [[ -f $(which flameshot) ]]; then
            time="$(date +%Y-%m-%d-%S)"
            flameshot full -p "$(xdg-user-dir SCREENSHOTS)/Screenshot_$time".png ; xdg-open "$(xdg-user-dir SCREENSHOTS)/Screenshot_$time".png
        elif [[ -f $(which grim) ]]; then
            time="$(date +%Y%m%d_%Hh%Mm%Ss_grim.png)"
            grim -c "$(xdg-user-dir SCREENSHOTS)/$time"  ; xdg-open "$(xdg-user-dir SCREENSHOTS)/$time"
        else
            msg
        fi
        ;;
    "$screen")
        if [[ -f $(which flameshot) ]]; then
            d=$(select_display) ; sleep  1 ; time="$(date +%Y-%m-%d-%S)" ; flameshot screen -p "$(xdg-user-dir SCREENSHOTS)/Screenshot_$time".png ; xdg-open "$(xdg-user-dir SCREENSHOTS)/Screenshot_$time".png
        elif [[ -f $(which grim) ]]; then
            time="$(date +%Y%m%d_%Hh%Mm%Ss_grim.png)"
            grim -cg "0,0 $(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/')" "$(xdg-user-dir SCREENSHOTS)/$time" 
            xdg-open "$(xdg-user-dir SCREENSHOTS)/$time"
        else
            msg
        fi
        ;;
    "$area")
        if [[ -f $(which flameshot) ]]; then
            time="$(date +%Y-%m-%d-%S)" ; flameshot gui -p "$(xdg-user-dir SCREENSHOTS)/Screenshot_$time".png ; xdg-open "$(xdg-user-dir SCREENSHOTS)/Screenshot_$time".png
        elif [[ -f $(which grim) ]]; then
            time="$(date +%Y%m%d_%Hh%Mm%Ss_grim.png)"
            grim -cg "$(slurp)" "$(xdg-user-dir SCREENSHOTS)/$time" ; xdg-open "$(xdg-user-dir SCREENSHOTS)/$time"
        else
            msg
        fi
        ;;
    "$window")
        if [[ -f $(which scrot) ]]; then
            scrot -u 'Screenshot_%Y-%m-%d-%S_$wx$h.png' -e 'mv $f $$(xdg-user-dir SCREENSHOTS) ; xdg-open $$(xdg-user-dir SCREENSHOTS)/$f'
        elif [[ -f $(which grim) ]]; then
            time="$(date +%Y%m%d_%Hh%Mm%Ss_grim.png)"
            grim -g "$(hyprctl activewindow | grep 'at: [0-9]' | grep -o -E '[0-9,]+') $(hyprctl activewindow | grep 'size: [0-9]' | grep -o -E '[0-9,]+' | sed s/,/x/)" "$(xdg-user-dir SCREENSHOTS)/$time"
            xdg-open "$(xdg-user-dir SCREENSHOTS)/$time"
        else
            msg
        fi
        ;;
    "$video")
        if [[ -f /usr/sbin/com.github.artemanufrij.screencast ]]; then
            /usr/sbin/com.github.artemanufrij.screencast
        elif [[ -f /usr/bin/simplescreenrecorder ]]; then
            /usr/bin/simplescreenrecorder
        elif [[ -f /usr/bin/wf-recorder ]]; then
            wf-recorder -ag "$(slurp)" --file="$(xdg-user-dir VIDEOS)/$(date +%Y%m%d_%Hh%Mm%Ss_record.mp4)"  >/dev/null 2>&1 &
            while [ ! -z $(pgrep -x wf-recorder) ]; do wait; done
            notify-send "Recording Complete" --hint int:transient:1
        fi
        ;;
esac
