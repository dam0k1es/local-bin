#!/usr/bin/env bash

rofi -no-lazy-grab -show drun -modi drun,filebrowser,ssh -theme ~/.config/rofi/launchpad.rasi -matching prefix
