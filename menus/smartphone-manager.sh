#!/usr/bin/env bash
set +x
set +e

RetVal="$(~/.config/polybar/scripts/polybar-kdeconnect.sh -d)"
command="$(echo $RetVal | awk -F ':' '{print $2}')"
[[ "$command" =~ "kdeconnect" ]] && eval "$command"
