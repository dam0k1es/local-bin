#!/usr/bin/env bash
# wifi-menu by sineto
# https://github.com/sineto/nmcli-rofi

# Send a notification as soon as a connection is established
notify() {
    if [[ "$(/bin/nmcli device status)" =~ "$chosen_network" ]] ;
    then
	      notify-send -t 2000 "Connected to $chosen_network" --hint int:transient:1
    else
	      notify-send -t 2000 "Connection failed" --hint int:transient:1
    fi
}

# Ask for a password to authenticate to the network
authenticate() {
    password=$(/bin/rofi -dmenu -i -p "Wifi password:" -theme ~/.config/rofi/text.rasi)
    nmcli device wifi connect "$chosen_network" password "$password"
}

# Obtain a list of all visible networks and sort them. Add an option
# to enable/disable network and show all options.
notify-send -t 1000 "Getting WiFi networks..." --hint int:transient:1
chosen_network=$(echo "$(nmcli -g SSID device wifi)" | sort | uniq | sed "$ s/$/\\nNetworking\ on\/off/" | sed '/^$/d' | rofi -dmenu -i -p "Wifi network" -no-custom -theme ~/.config/rofi/text.rasi)
status="$(nmcli networking connectivity)"

# If no connection was selected or no connections are be available exit the program
[[ "$status" == "full" ]] || exit 1
[[ -z "$chosen_network" ]] &&  exit 1

# Activate or deactivate the network components
if [[ "$chosen_network" == "Networking on/off" ]] ;
then
    [[ "$status" == "full" ]] && nmcli networking off
    [[ "$status" == "none" ]] && nmcli networking on
    exit
fi

# Establish a connection to an existing or new network
if [[ -n $(nmcli -g NAME connection | grep "$chosen_network") ]]; then
    nmcli connection up id "$chosen_network" || \
	      authenticate
    notify
else
    nmcli device wifi connect "$chosen_network" || \
	      authenticate
    notify
fi
