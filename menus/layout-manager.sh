#!/usr/bin/env bash
#set -x

launch_rofi() {
	local rofi_options
    rofi_options=(-dmenu -i -lines 3 -p "Workspaces to restore" -theme ~/.config/rofi/blurry.rasi -matching prefix )
	local launcher
    launcher=(rofi "${rofi_options[@]}") 
	local menu_options
    menu_options=$(printf '%s\n' "${!workspaces[@]}" | sort -n)
	#local menu_options="All\n$menu_options\nNone\n"
	rofi_selection="$(printf '%s' "$menu_options" | "${launcher[@]}")"
}

#Get Workspace Names, remove the "Workspace" Prefix / the layout|programs suffix and insert Whitespaces
fetch_current_workspaces() {
	local filter_ws_name_sed_regex="s/Workspace\s(.+)\s(layout|programs)/\1/I"
	while read -r workspace; do
		workspaces["$workspace"]="i3-resurrect restore -w $workspace"
	done < <(i3-resurrect ls workspaces | sed -r "$filter_ws_name_sed_regex" | sed 's/\(.*[0-9]\)/\1\ /' | sed 's/\([A-Z]\)/\ \ \1/g')
}

declare -A workspaces
fetch_current_workspaces
rofi_selection=""
launch_rofi

# Remove Whitespaces 
rofi_selection="$(echo "$rofi_selection" | sed s/\ //g)"

if [[ -n "$rofi_selection" ]]; then
	if [[ "$rofi_selection" == "None" ]]; then
		exit 1
	fi

	#if [[ "$rofi_selection" == "All" ]]; then
	#	for workspace in "${!workspaces[@]}"; do
	#		i3-resurrect restore -w "$workspace"
	#	done
    #
	#	exit 0
	#fi

	if [[ -v "workspaces[$rofi_selection]" ]]; then
		exec ${workspaces[${rofi_selection}]}
		exit 0
	fi

	IFS=' ' read -ra split_selection <<< "$rofi_selection"
	for selection in "${split_selection[@]}"; do
		i3-resurrect restore -w "$selection" 
	done
	exit 0
fi
