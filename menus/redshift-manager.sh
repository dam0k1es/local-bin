#!/usr/bin/env bash
set -x

launch_rofi() {
	  local rofi_options
    rofi_options=(-dmenu -i -lines 3 -p "Redshift Mode" -theme ~/.config/rofi/blurry.rasi -matching prefix )
    local launcher
    launcher=(rofi "${rofi_options[@]}")
	  local menu_options
    menu_options=$(printf '%s\n' "${!modes[@]}" | sort -n)
    rofi_selection="$(printf '%s' "$menu_options" | "${launcher[@]}")"
}


declare -A modes
modes[Auto]="redshift -P -t 4000:3000"
modes[Reset]="redshift -x"
modes[2500k]="redshift -P -O 2500"
modes[3500k]="redshift -P -O 3500"
modes[4500k]="redshift -P -O 4500"
rofi_selection=""
launch_rofi

if [[ -n "$rofi_selection" ]] ;
then
    [[ -v "modes[$rofi_selection]" ]] && \
        notify-send "Setting Redshift Mode" "${modes[$rofi_selection]}" \
                    -t 2000 --hint int:transient:1 && \
	      nohup ${modes[$rofi_selection]} &
fi
