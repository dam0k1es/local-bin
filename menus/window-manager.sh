#!/usr/bin/env bash

rofi -no-lazy-grab -show window -modi window -theme ~/.config/rofi/text.rasi
