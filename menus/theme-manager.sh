#!/usr/bin/env bash
#set -x

#Needs rofi

launch_rofi() {
    local rofi_options
    rofi_options=(-dmenu -i -lines 3 -p "theme to restore" -theme ~/.config/rofi/blurry.rasi -matching prefix )
    local launcher
    launcher=(rofi "${rofi_options[@]}")
    local menu_options
    menu_options=$(printf '%s\n' "${!themes[@]}" | sort -n)
    local menu_options="Configure\n$menu_options\n"
    rofi_selection="$(printf  "$menu_options" | "${launcher[@]}")"
}

fetch_current_themes() {
    while read -r theme; do
	themes["$theme"]=""
    done < <(wpg -l)
}

declare -A themes
fetch_current_themes
rofi_selection=""
launch_rofi

if [[ -n "$rofi_selection" ]] ;
then
    [[ "$rofi_selection" == "Configure" ]] && \
        wpg

    if [[ -v "themes[$rofi_selection]" ]] ;
    then
        #Notify
        notify-send --hint int:transient:1 -t 1000 "Setting Theme" "themes[$rofi_selection]"
        sleep 0.5

	# Set Colorscheme, GTK/QT Theme, Emacs/Polybar Theme
	# Needs wpgtk, xsettingsd, imagemagick
        ~/.local/bin/theme.sh "$rofi_selection"
    fi
fi
