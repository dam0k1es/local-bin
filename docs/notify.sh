# About
With this script, notifications can be sent via notify-send as soon as the volume or brightness has been changed. \
The script was developed for i3wm and dunst, but it also works for any other WMs or DEs.

# Installation

Make executable
```bash
chmod +x notify.sh
```
Move Script to Destination
```bash
cp notify.sh ~/.local/bin/notify.sh
```
Move img to icons folder
```bash
mkdir -p ~/.local/share/icons/
mv img ~/.local/share/icons/Notifications
```
Update Icon Cache
```bash
sudo gtk-update-icon-cache
```

Execute 
```bash
./notify.sh [volume|brightness]
```
 Now add to i3wm something like:
 ```bash
bindsym XF86AudioRaiseVolume exec "amixer set Master 5%+ ; ~/.local/bin/notify.sh volume"
bindsym XF86AudioLowerVolume exec "amixer set Master 5%- ; ~/.local/bin/notify.sh volume"
bindsym XF86AudioMute exec "amixer set Master toggle ; ~/.local/bin/notify.sh volume" 
bindsym XF86MonBrightnessUp exec "xbacklight -inc 5 ; ~/.local/bin/notify.sh brightness" 
bindsym XF86MonBrightnessDown exec "xbacklight -dec 5 ; ~/.local/bin/notify.sh brightness"
```

# Dependencys:
- xorg-xbacklight (xbacklight)
- alsa-utils (amixer)
- libnotify (notify-send)

# Optional:
- libcanberra (canberra-gtk-play)
- yaru-sound-theme (audio-volume-change.oga)
