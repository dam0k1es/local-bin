# local-bin

This repository contains various scripts that are useful for i3, Hyprland and possibly other environments. Some of them are not actively used anymore, as my system is changing all of the time. As I am mostly using Nix nowadays, most of the scripts will either be rewritten, or not maintained anymore.

### Contents:
- [theme.sh](theme.sh): Automatic theming of the desktop based on an image captured by wpgtk
- [meta-menu.sh](meta-menu.sh): Rofi menu that calls up the menus under [menus/](menus/) (Applications, Network, Theme, ...)
- [notify.sh](notify.sh): Script to output a notification along with an icon for brightness or volume, which it detects itself
- [BatteryAlert.sh](BatteryAlert.sh): Script that alerts when the battery level is too low when run as a daemon
- ...


### Usage:
``` bash
git clone https://gitlab.com/dam0k1es/local-bin /tmp/local-bin 
mv /tmp/local-bin/* $HOME/.local/bin/ ; rmdir /tmp/local-bin
echo 'PATH=$PATH:$HOME/.local/bin/' >> ~/.profile
```
