#!/usr/bin/env bash
# set -x

WALLPAPER_PATH="$HOME/Bilder/Wallpaper"
X_WMS=("i3" "openbox" "bspwm" "awesome")
WAYLAND_WMS=("hyprland" "sway" "wlroots" "river" "wayfire")
# DESKTOPS=(pop gnome unity xfce x-cinnamon mate budgie pantheon lxqt lxde plasma)

# Dependencies: wpgtk, xsettingsd, gsettings, qt5ct, ImageMagick, nitrogen (Xorg), swaybg (Wayland)

GTK_THEME=Adwaita
GTK_DARK_THEME=Adwaita-dark 
QT_THEME=Adwaita
QT_DARK_THEME=Adwaita-dark 

displayHelp() {                                                              
    echo "This script allows customization of the desktop theme and background image."                                                                    
    echo "Usage: $0 [THEME|IMAGE] [OPTION]"                                        
    echo "Options:"
    echo "  --tilix             : Adjusts the color scheme for Tilix."       
    echo "  --kitty             : Adjusts the color scheme for Kitty."       
    echo "  --cosmic            : Adjusts the color scheme for cosmic components."       
    echo "  --emacs             : Adjusts the color scheme for Emacs."
    echo "  --swaync            : Adjusts the color scheme for Swaync."
    echo "  --background        : Adjusts the background."
    echo "  --qt                : Adjusts the QT theming."
    echo "  --gtk               : Adjusts the GTK theming."
    echo "  --x                 : Adjusts the X11 theming."
    echo "  THEME               : Specifies the theme (light or dark)."
    echo "  IMAGE               : Path to the background image."
}

majorcol() {
    echo "Retrieving color from image $(echo "$1" | awk -F '/' '{print $NF}')" > /dev/tty
    # Check if the 'convert' command is available
    if command -v convert >/dev/null; then
        # Use 'convert' to get the dominant color from the image
        col=$(convert "$1" -scale 50x50! -depth 8 +dither -colors 8 -format "%c" histogram:info: | \
            sed -n 's/^[ ]*\(.*\):.*[#]\([0-9a-fA-F]*\) .*$/\1,#\2/p' | \
            sort -n | \
            tail -n 1 | \
            awk -F "," '{print $2}' | \
            sed 's/#/0x/' | \
            head -c 8)
    echo "  Retrieved $col from $(echo "$1" | awk -F '/' '{print $NF}')" > /dev/tty
    echo "$col"
    fi
}

evaluateThemeBrightness() {
    echo "Categorizing theme as dark or light" > /dev/tty
    # Extract hexadecimal values for the RGB components
    R=$(echo "$1" | cut -c3-4)
    G=$(echo "$1" | cut -c5-6)
    B=$(echo "$1" | cut -c7-8)

    # Convert hexadecimal values to decimal values
    R=$((16#$R))
    G=$((16#$G))
    B=$((16#$B))

    # Calculate the brightness of the theme based on the RGB values
    brightness=$(( (R * 299) + (G * 587) + (B * 114) ))
    brightness=$((brightness / 1000))

    # Evaluate the brightness of the theme and output the result
    if [[ brightness -gt 125 ]]; then
        echo "light"
        echo "  Categorized theme as light" > /dev/tty
    else
        echo "dark"
        echo "  Categorized theme as dark" > /dev/tty
    fi
    
}

adjustX11Theme() {
    echo "Setting X11 theme" > /dev/tty
    theme="$1"

    if [[ -w "$HOME/.config/xsettingsd/xsettingsd.conf" ]]; then
        if ! grep -q "^Net/ThemeName" "$HOME/.config/xsettingsd/xsettingsd.conf"; then
            echo "Net/ThemeName \"$GTK_THEME\"" >> "$HOME/.config/xsettingsd/xsettingsd.conf"
        fi
        if [[ "$theme" == "dark" ]]; then
            sed -i "/Net\/ThemeName/cNet\/ThemeName \"$GTK_DARK_THEME\"" "$HOME/.config/xsettingsd/xsettingsd.conf"
        elif [[ "$theme" == "light" ]]; then
            sed -i "/Net\/ThemeName/cNet\/ThemeName \"$GTK_THEME\"" "$HOME/.config/xsettingsd/xsettingsd.conf"
        fi

        if ! grep -q "^Gtk/ThemeName" "$HOME/.config/xsettingsd/xsettingsd.conf"; then
            echo "Gtk/ThemeName \"$GTK_THEME\"" >> "$HOME/.config/xsettingsd/xsettingsd.conf"
        fi
        if [[ "$theme" == "dark" ]]; then
            sed -i "/Gtk\/ThemeName/cGtk\/ThemeName \"$GTK_DARK_THEME\"" "$HOME/.config/xsettingsd/xsettingsd.conf"
        elif [[ "$theme" == "light" ]]; then
            sed -i "/Gtk\/ThemeName/cGtk\/ThemeName \"$GTK_THEME\"" "$HOME/.config/xsettingsd/xsettingsd.conf"
        fi
        
        xsettingsd -c ~/.config/xsettingsd/xsettingsd.conf 2>/dev/null &
        echo "  Set the X11 theme" > /dev/tty
    fi
}

adjustGTKTheme() {
    echo "Setting GTK theme" > /dev/tty
    theme="$1"

    # Check if GTK settings.ini files exist for gtk-3
    if [[ -f "$HOME/.config/gtk-3.0/settings.ini" ]]; then
        if ! grep -q 'prefer-dark' "$HOME/.config/gtk-3.0/settings.ini"; then
            echo "gtk-application-prefer-dark-theme=true" >> "$HOME/.config/gtk-3.0/settings.ini"
        fi
        if ! grep -q 'theme-name=' "$HOME/.config/gtk-3.0/settings.ini"; then
            echo "theme-name=$GTK_DARK_THEME" >> "$HOME/.config/gtk-3.0/settings.ini"
        fi
        # Set GTK theme and dark mode preference in gtk-3 settings.ini file
        if [[ "$theme" == "dark" ]]; then
            sed -i '/prefer-dark/cgtk-application-prefer-dark-theme=true' "$HOME/.config/gtk-3.0/settings.ini"
            sed -i "/theme-name/cgtk-theme-name=$GTK_DARK_THEME" "$HOME/.config/gtk-3.0/settings.ini"
        elif [[ "$theme" == "light" ]]; then
            sed -i '/prefer-dark/cgtk-application-prefer-dark-theme=false' "$HOME/.config/gtk-3.0/settings.ini"
            sed -i "/theme-name/cgtk-theme-name=$GTK_THEME" "$HOME/.config/gtk-3.0/settings.ini"
        fi
        echo "  Set the GTK3 theme" > /dev/tty
    fi
    
    # Check if GTK settings.ini files exist for gtk-4
    if [[ -f "$HOME/.config/gtk-4.0/settings.ini" ]]; then
        if ! grep -q 'prefer-dark' "$HOME/.config/gtk-4.0/settings.ini"; then
            echo "gtk-application-prefer-dark-theme=true" >> "$HOME/.config/gtk-4.0/settings.ini"
        fi
        if ! grep -q 'theme-name=' "$HOME/.config/gtk-4.0/settings.ini"; then
            echo "theme-name=$GTK_DARK_THEME" >> "$HOME/.config/gtk-4.0/settings.ini"
        fi
        # Set GTK theme and dark mode preference in gtk-4 settings.ini file
        if [[ "$theme" == "dark" ]]; then
            sed -i '/prefer-dark/cgtk-application-prefer-dark-theme=true' "$HOME/.config/gtk-4.0/settings.ini"
            sed -i "/theme-name/cgtk-theme-name=$GTK_DARK_THEME" "$HOME/.config/gtk-4.0/settings.ini"
        elif [[ "$theme" == "light" ]]; then
            sed -i '/prefer-dark/cgtk-application-prefer-dark-theme=false' "$HOME/.config/gtk-4.0/settings.ini"
            sed -i "/theme-name/cgtk-theme-name=$GTK_THEME" "$HOME/.config/gtk-4.0/settings.ini"
        fi
        echo "  Set the GTK4 theme" > /dev/tty
    fi


    # Check if gsettings is available
    if command -v gsettings >/dev/null; then
        # Set GTK theme and color scheme with gsettings
        if [[ "$theme" == "dark" ]]; then
            # Set GTK theme and color scheme 
            gsettings set org.gnome.desktop.interface gtk-theme "$GTK_DARK_THEME"
            gsettings set org.gnome.desktop.interface color-scheme prefer-dark
        elif [[ "$theme" == "light" ]]; then
            # Set GTK theme and color scheme
            gsettings set org.gnome.desktop.interface gtk-theme "$GTK_THEME"
            gsettings set org.gnome.desktop.interface color-scheme prefer-light
        fi
    echo "  Set the GTK theme via gsettings" > /dev/tty
    fi
}

adjustQTTheme() {
    echo "Setting QT theme" > /dev/tty
    theme="$1"

    # Check if qt5ct is available
    if command -v qt5ct >/dev/null; then
        # Set QT style and color scheme in qt5ct.conf
        if [[ "$theme" == "dark" ]]; then
            # Set QT style and color scheme
            sed -i "/style/cstyle='$QT_DARK_THEME'" "$HOME/.config/qt5ct/qt5ct.conf"
            sed -i "/color_scheme_path/ccolor_scheme_path='/usr/share/color-schemes/$QT_DARK_THEME.colors'" "$HOME/.config/qt5ct/qt5ct.conf"
        else
            # Set QT style and color scheme
            sed -i "/style/cstyle='$QT_THEME'" "$HOME/.config/qt5ct/qt5ct.conf"
            sed -i "/color_scheme_path/ccolor_scheme_path='/usr/share/color-schemes/$QT_THEME.colors'" "$HOME/.config/qt5ct/qt5ct.conf"
        fi
    echo "  Set the QT theme" > /dev/tty
    fi
}

adjustTilixColors() {
    # Check if Tilix is installed
    if command -v tilix >/dev/null; then
        echo "Setting tilix theme" > /dev/tty
        THEME="$1"

        # Dump current Tilix color scheme to a temporary file
        dconf dump /com/gexperts/Tilix/ > /tmp/tilix
        CURRENT_COLOR=$(sed -n "s/.*background-color='#\([0-9A-Fa-f]\{6\}\)'.*/\1/p" /tmp/tilix)

        # Check if current color is not found, and add it if not
        if [[ -z "$CURRENT_COLOR" ]]; then
            # Color not found, add it
            echo "background-color='#262727'" >> /tmp/tilix
        else
            # Check if current color is 262727, and replace it accordingly
            if [[ "$THEME" == "light" ]]; then
                sed -i "s/262727/ECE8DF/" /tmp/tilix
            elif [[ "$THEME" == "dark" ]]; then
                sed -i "s/ECE8DF/262727/" /tmp/tilix
            fi
        fi

        # Load updated Tilix color scheme
        dconf load /com/gexperts/Tilix/ < /tmp/tilix
        echo "  Set the tilix theme" > /dev/tty
    fi
}

adjustKittyColors() {
    # Check if Kitty is installed
    if command -v kitty >/dev/null; then
        echo "Setting kitty theme" > /dev/tty
        THEME="$1"
            # Check if current color is 262727, and replace it accordingly
            if [[ "$THEME" == "light" ]]; then
                sed -i "s/background #262727/background #ECE8DF/" ~/.config/kitty/kitty.conf
                wal -e -n --theme ashes -l
            elif [[ "$THEME" == "dark" ]]; then
                sed -i "s/background #ECE8DF/background #262727/" ~/.config/kitty/kitty.conf
                wal -e -n --theme ashes
            fi
    fi

    kill -SIGUSR1 $(pidof kitty)
} 

adjustCosmicColors() {
    # Check if Cosmic is installed
    if [ -d ~/.config/cosmic ] ; then
        echo "Setting cosmic theme" > /dev/tty
        THEME="$1"
            # Check if current color is 262727, and replace it accordingly
            if [[ "$THEME" == "light" ]]; then
                sed -i "s/true/false/" ~/.config/cosmic/com.system76.CosmicTheme.Mode/v1/is_dark
            elif [[ "$THEME" == "dark" ]]; then
                sed -i "s/false/true/" ~/.config/cosmic/com.system76.CosmicTheme.Mode/v1/is_dark
            fi
    fi
}

adjustEmacsTheme() {
    # Check if Emacs is installed
    if command -v emacs >/dev/null; then
        echo "Setting emacs theme" > /dev/tty

        # Check the current theme based on input (dark or light)
        if [[ "$1" == "dark" ]]; then
            local THEME='spacemacs-dark'
            local OLDTHEME='spacemacs-light'
        elif [[ "$1" == "light" ]]; then
            local THEME='spacemacs-light'
            local OLDTHEME='spacemacs-dark'
        fi

        # Cycle through spacemacs themes for all sockets 
        emacs_server_list="$(pgrep emacs -a | awk -F '=' '/daemon/{print $NF}')"
        for server in $emacs_server_list; do
            emacsclient --socket-name "$server" --eval "(load-theme '$THEME t)" &>/dev/null
            echo "  Set the emacs theme for server $server" > /dev/tty
        done
            
        emacsclient --eval "(load-theme '$THEME t)" &>/dev/null
        echo "  Set the emacs theme for default server" > /dev/tty

        # Replace spacemacs-dark with spacemacs-light and vice versa in 
        # .spacemacs if .spacemacs file exists and is writable
        if [[ -f ~/.spacemacs && -w ~/.spacemacs ]]; then
            sed -i "s/$OLDTHEME/$THEME/" ~/.spacemacs
            echo "  Set the emacs theme in ~/.spacemacs" > /dev/tty
        fi

        # Replace spacemacs-dark with spacemacs-light and vice versa in the 
        # emacs settings if the file exists and is writable
        if [[ -f ~/.emacs.d/settings.org && -w ~/.emacs.d/settings.org ]]; then
            SETTINGS_FILE=~/.emacs.d/settings.org
            echo "  Set the emacs theme in ~/.emacs.d/settings.org" > /dev/tty
        elif [[ -f ~/.emacs.d/settings.el && -w ~/.emacs.d/settings.el ]]; then
            SETTINGS_FILE=~/.emacs.d/settings.el
            echo "  Set the emacs theme in ~/.emacs.d/settings.el" > /dev/tty
        fi
        if [[ -n "$SETTINGS_FILE" ]]; then
            sed -i "s/$OLDTHEME/$THEME/" "$SETTINGS_FILE"
        fi

    fi
}

# TODO: TEST THIS
adjustSwayncTheme() {
    if command -v swaync >/dev/null; then
        echo "Setting swaync theme" > /dev/tty
        # Check if the style.css file exists and is writable
        if [[ -f ~/.config/swaync/style.css && -w ~/.config/swaync/style.css ]]; then
            # Replace the appropriate color based on the theme
            if [[ "$THEME" == "dark" ]]; then
                sed -i 's/rgba(41, 43, 46, 0.8)/rgba(251, 248, 239, 0.8)/' ~/.config/swaync/style.css
            elif [[ "$THEME" == "light" ]]; then
                sed -i 's/rgba(251, 248, 239, 0.8)/rgba(41, 43, 46, 0.8)/' ~/.config/swaync/style.css
            fi
        fi

        # Restart swaync Wayland-based window manager
        for wm in "${WAYLAND_WMS[@]}"; do
            if [[ "$DESKTOP_SESSION" == "$wm" ]];  then
                swaync-client -rs
            fi
        done
        echo "  Set the swaync theme" > /dev/tty
    fi
}

setBackground() {
    # Check if wpg is installed
    if command -v wpg >/dev/null; then
        echo "Setting the background" > /dev/tty
        # Set variables
        image="$1"
        background="$HOME/.config/wpg/wallpapers/$image"

        # Exit if the background image file does not exist
        [[ -f "$background" ]] || exit

        # Set background image based on the desktop environment
        for wm in "${X_WMS[@]}"; do
            if [[ "$DESKTOP_SESSION" == "$wm" ]] && [[ $(command -v nitrogen 2>/dev/null) != "" ]] && [[ $(command -v xrandr 2>/dev/null) != "" ]]; then
                nitrogen --set-scaled "$background" --head=0
                [[ $(xrandr --listactivemonitors) =~ "HDMI" ]] && nitrogen --set-scaled "$background" --head=1
            fi
        done

        # Set background image for Wayland-based window manager
        for wm in "${WAYLAND_WMS[@]}"; do
            if [[ "$DESKTOP_SESSION" == "$wm" ]] && [[ $(command -v swaybg 2>/dev/null) != "" ]]; then
                swaybg -i "$background" &
            fi
        done
        echo "  Set the background" > /dev/tty
    fi
}

if [[ $# -eq 0 || "$1" == "--help" ]]; then
    displayHelp
    exit 0
else
    # Check if the first parameter is light or dark
    if [[ "$1" == "light" || "$1" == "dark" ]]; then
        THEME="$1"
        shift
    else
        # If the first parameter is an image
        IMAGE="$1"
        COLOR="$(majorcol "$WALLPAPER_PATH"/"$IMAGE")"
        THEME="$(evaluateThemeBrightness "$COLOR")"
        shift
    fi
fi

if [[ $# == 0 ]]; then
    adjustTilixColors "$THEME"
    adjustKittyColors "$THEME"
    adjustCosmicColors "$THEME"
    adjustEmacsTheme "$THEME"
    adjustSwayncTheme "$THEME"
    adjustQTTheme "$THEME"
    adjustGTKTheme "$THEME"
    adjustX11Theme "$THEME"
    setBackground "$IMAGE"
else
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --tilix) adjustTilixColors "$THEME" ;;
            --kitty) adjustKittyColors "$THEME" ;;
            --cosmic) adjustCosmicColors "$THEME" ;;
            --emacs) adjustEmacsTheme "$THEME" ;;
            --swaync) adjustSwayncTheme ;;
            --qt) adjustQTTheme "$THEME";;
            --gtk) adjustGTKTheme "$THEME";;
            --x) adjustX11Theme "$THEME";;
            --background) setBackground "$IMAGE" ;;
            *) displayHelp ;;
        esac
        shift
    done
fi
