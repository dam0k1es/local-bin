#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

## Modification : Dam0k1es
## Added kblock mode with transparent i3lock

dir="$HOME/.config/rofi/"
rofi_command="rofi -theme $dir/powermenu.rasi"

uptime=$(uptime -p | sed -e 's/up //g')

# Options
shutdown="󰐥"
reboot="↻"
lock="⚿"
kblock="󰌌"
suspend="󰒲"
logout="󰗽"

# Confirmation
confirm_exit() {
    rofi -dmenu\
        -i\
        -no-fixed-num-lines\
        -p "Are You Sure? : "\
        -theme "$dir"/confirm.rasi
    }

# Message
msg() {
    rofi -theme "$dir/message.rasi" -e "Available Options  -  yes / y / no / n"
}

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$kblock\n$suspend\n$logout"

chosen="$(echo -e "$options" | $rofi_command -p "Uptime: $uptime" -dmenu -selected-row 2)"

case $chosen in
    "$shutdown")
        ans=$(confirm_exit &)
        if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
            systemctl poweroff
        elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
            exit 0
        else
            msg
        fi
        ;;
    "$reboot")
        ans=$(confirm_exit &)
        if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
            systemctl reboot
        elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
            exit 0
        else
            msg
        fi
        ;;
    "$lock")  
        if [[ "$DESKTOP_SESSION" = "i3" ]]; then
            # systemctl suspend
            ~/.local/bin/lock.sh no-wait force
        elif [[ "$DESKTOP_SESSION" = "hyprland" ]]; then
            # systemctl suspend
            ~/.local/bin/lock.sh no-wait force 
        fi
        ;;
    "$kblock")
        if [[ -f $( which swaylock ) ]]; then
            ~/.local/bin/lock.sh no-wait force Swaylock-transparent
        fi
        if [[ -f $( which i3lock ) ]]; then
            ~/.local/bin/lock.sh no-wait force I3lock-transparent
        fi
        ;;
    "$suspend")
        ans=$(confirm_exit &)
        if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
            systemctl suspend
            ~/.local/bin/lock.sh no-wait force 
        elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
            exit 0
        else
            msg
        fi
        ;;
    "$logout")
        ans=$(confirm_exit &)
        if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
            if [[ "$DESKTOP_SESSION" == "Openbox" ]]; then
                openbox --exit
            elif [[ "$DESKTOP_SESSION" == "bspwm" ]]; then
                bspc quit
            elif [[ "$DESKTOP_SESSION" == "i3" ]]; then
                i3-msg exit
            elif [[ "$DESKTOP_SESSION" == "hyprland" ]]; then
                pkill Hyprland
            fi
        elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
            exit 0
        else
            msg
        fi
        ;;
esac
