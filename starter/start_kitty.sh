#!/usr/bin/env bash
#launch kitty with environment variables
#this allows us to insert ^, ` and ´
kitty --detach --hold -e zsh
