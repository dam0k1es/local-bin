#!/usr/bin/env bash

set -euo pipefail

# Default values for command line arguments
wait="nowait"  # Default behavior: don't wait before locking
mode="normal"  # Default behavior: normal locking mode, no force
locker="Swaylock"      # No default locker set; it will be determined based on available software

# Process command line arguments if present
if [ "$#" -ge 1 ]; then
    wait="$1"
fi
if [ "$#" -ge 2 ]; then
    mode="$2"
fi
if [ "$#" -ge 3 ]; then
    locker="$3"
fi

# Define the number of cycles for dimming the screen
lockcycles=10
lockerFound=false

# Function definitions

# Function to wait until a process with the given name exists
await() {
    local process="$1"
    
    while true; do
        # Check if the process exists
        if pgrep -x "$process" > /dev/null; then
            echo "Process '$process' exists."
            break
        else
            echo "Waiting for process '$process' to start..."
            sleep 1 
        fi
    done

    sleep 1
}

# Check if any supported screen locker command exists
check_locker_exists() {
    for lockCmd in dm-tool i3lock swaylock hyprlock; do
        if command -v "$lockCmd" &> /dev/null; then
            lockerFound=true
            break
        fi
    done

    if ! $lockerFound; then
        echo "No supported locker found."
        exit 1
    fi
}

# Check if the screen is already locked
# Pgrep with -f to match full commandlines (NixOS path matching)
# (Codenames start with upper case, programs with lower case)
check_screen_locked() {
    if pgrep -f "swaylock|i3lock|dm-tool|hyprlock" &> /dev/null; then
        echo "Screen is already locked."
        exit 1
    fi
}

# Check if media is currently playing, exit unless forced
check_media_playing() {
    if [[ "$(playerctl status -a 2>/dev/null)" =~ "Playing" ]] && [ "$mode" != "force" ]; then
        echo "Media playback is active."
        exit 1
    fi
}

# Check if specified applications are active
# Pgrep with -f to match full commandlines (NixOS path matching)
check_apps_active() {
    local apps=("zoom" "virt-manager" "freetube" "spotify")
    for app in "${apps[@]}"; do
        if [[ $(pgrep -f "$app") ]] && [ "$mode" != "force" ]; then
            echo "$app is active."
            exit 1
        fi
    done
}

# Pause or resume notifications depending on the locker state
toggle_notifications() {
    if command -v swaync &> /dev/null; then
        swaync-client -d -sw
    elif command -v dunstctl &> /dev/null; then
        dunstctl set-paused toggle
    fi
}

# Gradually dim the screen before locking
# Might cause trouble (light does not work all the time)
dim_screen_before_lock() {
    local mtool="" cursorpos="" lightlevel=""
    if [ "$wait" == "wait" ]; then
        for tool in hyprctl xdotool; do
            if command -v "$tool" &> /dev/null; then
                mtool="$tool"
                break
            fi
        done
        if [ "$mtool" ] && command -v light &> /dev/null; then
            cursorpos=$($mtool cursorpos 2>/dev/null || true)
            lightlevel=$(echo $(light) / 1 | bc) 
            for i in $(seq 1 $lockcycles); do
                sleep 0.25
                light -U $(($lightlevel/$lockcycles))
                if [ "$($mtool cursorpos 2>/dev/null || true)" != "$cursorpos" ]; then
                    light -S "$lightlevel"  # Reset light level if movement is detected
                    exit 0
                fi
            done
        fi
    fi
    echo "$lightlevel"
}

# Lock the screen using the specified locker method
lock_screen() {
    case "$locker" in
        Dm-tool)
            dm-tool lock & # Lock using dm-tool
            await dm-tool
            light -S "$lightlevel" # Return to normal lightlevel
            ;;
        Hyprlock)
            hyprlock & # Lock using hyprlock
            await hyprlock
            light -S "$lightlevel" # Return to normal lightlevel
            ;;
        I3lock-transparent)
            i3lock --color=000000 --ignore-empty-password --show-failed-attempts  # Lock with i3lock-transparent
            ;;
        Swaylock-transparent)
            swaylock --color=00000000 --ignore-empty-password --show-failed-attempts  # Lock with swaylock-transparent
            ;;
        I3lock)
            i3lock --color=000000 --image "/home/$USER/Bilder/Wallpaper/lock.png" --ignore-empty-password --show-failed-attempts & # Lock with i3lock
            await i3lock
            light -S "$lightlevel" # Return to normal lightlevel
                ;;
        Swaylock)
            swaylock --color=000000 --ignore-empty-password --show-failed-attempts  & # Lock with swaylock
            await swaylock
            light -S "$lightlevel" # Return to normal lightlevel
            ;;
        *)
            echo "Unknown locker: $locker"
            exit 1
            ;;
    esac
}

# Main script logic
check_locker_exists
check_screen_locked
check_media_playing
check_apps_active
toggle_notifications  # Disable notifications
lightlevel=$(dim_screen_before_lock)
lock_screen  # Execute the screen locking
toggle_notifications  # Re-enable notifications
