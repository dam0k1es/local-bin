#!/usr/bin/env bash

API_KEY=$(gpg --quiet --decrypt ~/.openai.txt.gpg)

select_model() {
    if [ $# -eq 1 ]; then
        choice=$1
    else
        clear
        echo "Wähle ein Modell:"
        echo "1. gpt3 (GPT-3.5-Turbo)"
        echo "2. gpt4 (GPT-4)"
        read -rp "Auswahl (1-2): " choice
    fi
    case $choice in
        gpt3|1)
            MODEL="gpt-3.5-turbo"
            ;;
        gpt4|2)
            MODEL="gpt-4"
            ;;
        *)
            echo "Ungültige Auswahl. Bitte wähle 1 oder 2."
            exit 1
            ;;
    esac
}

ask() {
    local content="$1"
    QUERY=$(jq -n \
                    --arg model "$MODEL" \
                    --arg content "$content" \
                    '{ "model": $model, "messages": [{"role": "user", "content": $content}] }')

    RESPONSE=$(curl -s -X POST "https://api.openai.com/v1/chat/completions" \
                -H "Content-Type: application/json" \
                -H "Authorization: Bearer $API_KEY" \
                --data "$QUERY")

    echo "$RESPONSE" | jq -r '.choices[0].message.content'
}

chat() {
    local context=""
    clear
    date >> ~/.chat_history
    while true; do
        echo -n ">_: "
        read -r input
        if [[ "$input" == "clear" ]]; then
            clear
            context=""
            echo "Chatverlauf wurde gelöscht."
            continue
        elif [[ "$input" == "exit" ]]; then
            break 
        fi
        content="$context$input"
        answer=$(ask "$content")
        echo "$MODEL: $answer"
        context="$content\n$MODEL: $answer\n"
    done
    echo -e "$context\n" >> ~/.chat_history
}

image() {
    prompt="$1"
    size="$2"
    response=$(curl -sS "https://api.openai.com/v1/images/generations" \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer $API_KEY" \
        -d '{
            "prompt": "'"${prompt}"'",
            "n": 1,
            "size": "'"$size"'"
        }')

    echo "$response"
    image_url=$(echo "$response" | jq -r '.output[0].image')
    curl -o generated_image.png "$image_url"
}

help() {
    echo "Benutzung: ./chat [-h] [-c] [-l] [-g MODEL] [-m MESSAGE] [-s SIZE] [-v PROMPT]"
    echo "Optionen:"
    echo "  -h         Zeigt diese Hilfe an."
    echo "  -c         Startet eine Chat-Sitzung."
    echo "  -l         Zeigt eine Liste der verfügbaren GPT-Modelle an."
    echo "  -g MODEL   Wähle das Modell aus: 'gpt3' für GPT-3.5-Turbo oder 'gpt4' für GPT-4."
    echo "  -m MESSAGE Die Nachricht, die dem Modell gesendet werden soll."
    echo "  -s SIZE    Die Größe des generierten Bildes: '256x256', '512x512', '1024x1024', usw."
    echo "  -i PROMPT  Die Beschreibung für das zu generierende Bild."
}

if [ $# -eq 0 ]; then
    ./chat -h
    exit 0
fi

while getopts ":clhg:m:s:i:" opt; do
    case $opt in
        c)
            select_model
            chat
            exit 0
            ;;
        l)
            echo "Verfügbare Modelle:"
            echo "1. gpt3 (GPT-3.5-Turbo)"
            echo "2. gpt4 (GPT-4)"
            exit 0
            ;;
        h)
            help
            exit 0
            ;;
        g)
            select_model "$OPTARG"
            ;;
        m)
            MESSAGE="$OPTARG"
            ;;
        s)
            SIZE="$OPTARG"
            ;;
        i)
            prompt="$OPTARG"
            image "$prompt" "$SIZE"
            exit 0
            ;;
        \?)
            echo "Ungültige Option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG erfordert ein Argument." >&2
            exit 1
            ;;
    esac
done

if [ -z "$MODEL" ] || [ -z "$MESSAGE" ]; then
    echo "Benutze alle Optionen: -g für Modell und -m für Nachricht."
    exit 1
fi

answer=$(ask "$MESSAGE")
echo "$MODEL: $answer"
