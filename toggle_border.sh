#!/usr/bin/env bash
set -x

# Get Window ID and Name
focused_id="$(xprop -root  | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')"
focused_name="$(xprop -id $focused_id | awk -F '=' '/WM_ICON_NAME\(STRING\)/{print $NF}')"

# Generate random "window id"
random_name="$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 16)"

# Set i3 Name to a known string
i3-msg [ id="$focused_id" ] title_format "$random_name"

# Get Border Width from i3-tree
current_border_width="$(i3-msg -t get_tree | jq |grep $random_name -B50 | awk '/current_border_width/{print ($NF+0)}' | tail -n 1)"

# Get Border Style from i3-tree
# current_border_style="$(i3-msg -t get_tree | jq |grep $random_name -B50 | awk  '/border"/{a=$0}END{print a}' | awk -F '"' '{print $4}')"

#Restore Window Name
i3-msg [id="$focused_id" ] title_format "$focused_name"

# Set Border Width and Style
# If the Border is set remove it
[ "$current_border_width" -eq 5 ] && \
i3-msg [ id="$focused_id" ] floating disable && \
i3-msg [ id="$focused_id" ] border pixel 0


# If there is no border add it
[ "$current_border_width" -le 2 ] && \
    i3-msg [ id="$focused_id" ] floating enable && \
    i3-msg [ id="$focused_id" ] border pixel 5
