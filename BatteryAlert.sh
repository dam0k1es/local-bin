#!/usr/bin/env bash

interval=60
soundfile=/usr/share/sounds/deepin/stereo/power-unplug.wav

while true; do
    total_percentage=0
    battery_count=0
    cable_connected="no"

    # Query all batteries
    for BATTERY in $(upower -e | grep 'BAT'); do
        BATTERY_PERCENTAGE=$(upower -i "$BATTERY" | grep percentage | awk '{ print $2 }' | sed 's/%//g')
        CABLE=$(upower -d | grep -A 5 -B 5 "$BATTERY" | grep -m 1 line-power | awk '{ print $3 }')

        total_percentage=$((total_percentage + BATTERY_PERCENTAGE))
        battery_count=$((battery_count + 1))

        # Check if the power cable is connected
        if [[ "$CABLE" = "yes" ]]; then
            cable_connected="yes"
        fi
    done

    # Calculate average
    if [[ $battery_count -gt 0 ]]; then
        average_percentage=$((total_percentage / battery_count))
    else
        average_percentage=0
    fi

    echo "Average Battery Percentage = $average_percentage, Cable Connected = $cable_connected"

    # Issue warning if the average is below 10%
    if [[ "$average_percentage" -lt "10" && "$cable_connected" = "no" ]]; then
        notify-send --urgency=critical "Battery is about to die" "Plug in the power cable"
        aplay "$soundfile"

        # Reduce interval if the average is below 5%
        if [[ "$average_percentage" -lt "5" ]]; then
            (( interval /= 2 ))
        fi
    fi

    sleep "$interval"
done

