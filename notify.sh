#!/usr/bin/env bash

directory=$HOME/.local/bin/media/

snddir=$HOME/.local/bin/media

brightness() {
    # Get raw backbrightness value
    [ -f "$(which xbacklight)" ] && raw="$(xbacklight -get)"
    [ "$WAYLAND_DISPLAY" != "" ] && [ -f "$(which light)" ] && raw="$(light)"
    # Cut value to int
    val="${raw%.*}"

    # Set Icon
    [ "$val" -gt "0"  ] && [ "$val" -le "20" ] &&  icon="$directory"/brightness-lowest.svg
    [ "$val" -gt "20" ] && [ "$val" -le "40" ] &&  icon="$directory"/brightness-low.svg
    [ "$val" -gt "40" ] && [ "$val" -le "60" ] &&  icon="$directory"/brightness-medium.svg
    [ "$val" -gt "60" ] && [ "$val" -le "80" ] &&  icon="$directory"/brightness-high.svg
    [ "$val" -gt "80" ] && [ "$val" -le "100" ] && icon="$directory"/brightness-full.svg
}

volume() {
    # Check if muted
    output="$(grep -oPm1 '(?<=\] \[).*(?=\])' <<< "$(amixer get Master)")"
    # Get volume value
    [ "$output" = "off" ] && val=0 || \
    val="$(grep -oPm1 '\d+(?=%)' <<< "$(amixer get Master)")"

    # Set Icon
    [ "$val" = "0" ] || [ "$output" = "off" ] && icon="$directory"/notification-audio-volume-muted.svg
    [ "$val" -gt "0"  ] && [ "$val" -lt "33" ] && icon="$directory"/notification-audio-volume-low.svg
    [ "$val" -gt "33" ] && [ "$val" -lt "66" ] && icon="$directory"/notification-audio-volume-medium.svg
    [ "$val" -gt "66" ] && icon="$directory"/notification-audio-volume-high.svg

    # Set Sound
    snd="$snddir"/audio-volume-change.oga
}

[ "$1" = "volume" ] && volume
[ "$1" = "brightness" ] && brightness
[ "$val" = "" ] && echo "Enter: '" "$0" "volume/brightness'" && exit

# Play Sound
[ "$snd" != "" ] && canberra-gtk-play -f "$snd" &

# Send Value, Bar and Icon
[[ "$(pgrep dunst)" != "" ]] && \
    notify-send -u low -t 500 "$val %" -h int:value:"$val" \
                -h string:x-dunst-stack-tag:"indicator" -i "$icon" \
                --hint int:transient:1

if [[ "$(pgrep swaync)" != "" ]]; then
    [ -f /tmp/notify ] && swaync-client --close-latest
    notify-send -u low -t 500 "$val %" -h int:value:"$val" -i "$icon" --hint int:transient:1
    touch /tmp/notify
    ( sleep 5 ; rm /tmp/notify ) &
fi
