#2020-11-07 :: Gideon Wolfe :: https://gideonwolfe.com/posts/workflow/i3/picom/
#Modification: Dam0k1es

set -x

#!/bin/bash
#if [ "$(ps -aux | grep [p]icom_minimal | grep -v grep | grep -v vim )" = "" ];
if (($(ps -aux | grep [p]icom | grep -v vim | wc -l) > 0))
then
  polybar-msg hook blur-toggle 1
  pkill -9 picom ; sleep 0.1
#  picom -b  --experimental-backends --config ~/.config/picom/picom_minimal.conf
else
  polybar-msg hook blur-toggle 2
  pkill -9 picom ; sleep 0.1
  picom -b  --experimental-backends --config ~/.config/picom/picom.conf
fi
