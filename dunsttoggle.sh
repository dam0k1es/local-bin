#!/usr/bin/env bash

[ "$(dunstctl is-paused)" == "false" ] && \
    dunstctl set-paused true && polybar-msg hook dunst 2 && exit
[ "$(dunstctl is-paused)" == "true" ] && \
    dunstctl set-paused false && polybar-msg hook dunst 1 && exit
