#!/run/current-system/sw/bin/bash

# Find out with evtest

device=event8 

if [[ $(pgrep evtest) = "" ]] ;
then
	evtest --grab /dev/input/"$device" > /dev/null
else
	pkill evtest
fi

