#!/usr/bin/env bash
set +e

# Usage: ./wpg_color.sh -i/-p/-r

getcolor() {
    #Get old Color String
    old="$(grep -o -P -m1 '(?<=#).*(?=;)' ~/.config/rofi/wpg.rasi)"

    #Get Theme from wpg
    img="$(cat ~/.cache/wal/wal | awk -F "/" '{print $NF}')"
    img_="$(echo $img | sed s/\\./_/)"

    #Get new String from wpg (with 6 as accent color)
    new=$(grep -o -P -m1 '(?<=color6": "#).*(?=")' /home/$USER/.config/wpg/schemes/_home_"$USER"__config_wpg_wallpapers_"$img_"_dark_wal__1.1.0.json)
}

getcolor

while getopts "ipr" o; do
    case "${o}" in
        i)
            #Print the Image
            echo "$img"
            ;;
        p)
            #Print the Color
            echo "$new"
            ;;
        r)
            #Replace Rofi Color with new Color
            sed s/"$old"/"$new"/g ~/.config/rofi/wpg.rasi -i
            ;;
    esac
done
