#!/usr/bin/env bash

if [ -n "$DESKTOP_SESSION" ];then
   for env_var in $(gnome-keyring-daemon --start --components=pkcs11,secrets,ssh); do
       export env_var
   done
fi
