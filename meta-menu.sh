#!/usr/bin/env bash
#set -x

trap "polybar-msg hook meta-menu 1" EXIT HUP INT QUIT PIPE TERM

# Set Polybar Module to "Open Menu"
[ "$(pgrep -x polybar)" != "" ] && polybar-msg hook meta-menu 2

launch_rofi() {
	local rofi_options
    rofi_options=(-dmenu -i -lines 3 -p "" -theme ~/.config/rofi/meta.rasi -matching prefix)
	local launcher
    launcher=(rofi "${rofi_options[@]}")
	local menu_options
    menu_options=$(printf '%s\n' "${!menus[@]}" | sed s/-manager.sh//g | sort -n)
    rofi_selection="$(printf %s "$menu_options" | "${launcher[@]}")"
}

fetch_current_menus() {
	while read -r menu; do
		menus["$menu"]="$menu"
	done < <(ls "$mdir")
}

mdir=~/.local/bin/menus/
declare -A menus
fetch_current_menus
rofi_selection=""
launch_rofi

if [[ -n "$rofi_selection" ]]; then
    if [[ -v "menus[${rofi_selection}-manager.sh]" ]]; then
        ( exec $mdir/"${menus[${rofi_selection}-manager.sh]}" )
        exit 0
    fi
fi
