#!/usr/bin/env bash

# Credits to: https://gist.github.com/dAnjou/b99f55de34b90246f381e71e3c8f9262

# Install: libsecret

# Store password in gnome-keyring:
# secret-tool store --label="KeePassXC" keepassxc keys
# Password: ...

# Unlock KeePassXC with gnome-keyring:
secret-tool lookup keepassxc keys | keepassxc --pw-stdin /home/damokles/Dokumente/.keys/Passwörter.kdbx --keyfile /home/damokles/Dokumente/.keys/keyfile
